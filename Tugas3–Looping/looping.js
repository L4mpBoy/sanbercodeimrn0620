// 1. Looping
var a = 0;
var b = 20;

console.log('LOOPING PERTAMA');
while (a < 20) {
	a = a+2;
	console.log(a+' - I love coding');
}
console.log('LOOPING KEDUA');
while (b > 2) {
	console.log(b+' - I will become a mobile developer');
	b = b-2;
}

// 2. Looping menggunakan for
for (var i = 1; i <= 20; i++) {
	if (i%2 == 1 && i != 3 && i != 9 && i != 15) {
		console.log(i+" - Santai");
	}
	if (i%2 == 0) {
		console.log(i+" - Berkualitas");
	}
	if (i == 3 || i ==9 || i == 15) {
		console.log(i+" - I Love Coding");
	}	
}

// 3. Membuat Persegi Panjang
for (var pp = 1; pp <= 4; pp++) {
	console.log("########");
}

// 4. Membuat Tangga
var n = '';

for (var j = 1; j <= 7; j++) {
		n += '#';
		console.log(n);
	}

// 5. Membuat Papan Catur
var p = '';
var h = '';

for (var bt = 1; bt <= 8; bt++) {
	if (bt%2 == 0) {
		console.log('# # # #');
	}
	if (bt%2 == 1) {
		console.log(' # # # #');
	}
}