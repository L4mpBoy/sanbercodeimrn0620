// Release 0
class Animal {
    // Code class di sini
    constructor(name){
    	this.name = name
    	this.legs = 4
    	this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


// Release 1
// Code class Ape dan class Frog di sini
class Ape {
	constructor(name) {
		this.legs = 2
		this.name = name
		this.yell = "Auooo"
		console.log(sungokong.yell)
	}
}

class Frog {
	constructor(name) {
		this.legs = 4
		this.name = name
		this.jump = "hop hop"
		console.log(kodok.jump)
	}
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// Function to Class
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 