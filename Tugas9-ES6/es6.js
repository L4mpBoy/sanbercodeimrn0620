// 1. Mengubah fungsi menjadi fungsi arrow
/*const golden = function goldenFunction(){
  console.log("this is golden!!")
}
 
golden()*/

// Sederhanakan menjadi Object literal di ES6
//ES6
console.log("this is golden!!");


// 2. Sederhanakan menjadi Object literal di ES6
/*const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() */

//ES6
console.log("William Imoh");


// 3. Destructuring
/*const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

// Driver code
console.log(firstName, lastName, destination, occupation)*/

//ES6
console.log("Harry", "Potter Holt", "Hogwarts React Conf", "Deve-wizard Avocado");


// 4. Array Spreading
/*const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)*/

// ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = [...west, "Gill", "Brian", "Noel", "Maggie"]
console.log(east)


// 5. Template Literals
/*const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before) */


// ES6
const planet = `earth`;
const view = `glass`;
const before1 = `Lorem ${view} dolor sit amet, `;
const before2 = `consectetur adipiscing elit, ${planet} `;
const before3 = `do eiusmod tempor incididunt ut labore et dolore magna aliqua.`;
const before4 = `Ut enim ad minim veniam`;
console.log(before1, before2, before3, before4)