// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let myTime = 10000;

function timeCount(num) {
	if(num == books.length) {
		return 0;
	} else {
		readBooks(myTime, books[num], function(check) {
			myTime = books[num].timeSpent;
			timeCount(num+1);
		});
	}
	
}
timeCount(0);