// if-else
console.log("If-Else ============>>")
var nama = "John";
var peran = "Penyihir"; // Peran Penyihir, Guard, dan Werewolf

if (nama == ""){
	console.log("Nama harus diisi!");
}else if (peran == ""){
	console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
}else if (peran == "Penyihir") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
}else if (peran == "Guard") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if (peran == "Werewolf") {
	console.log("Selamat datang di Dunia Werewolf, "+nama);
	console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!")
}
console.log("\n")

// Switch Case
console.log("Switch ============>>")
var tanggal = 17; 
var bulan = 8; 
var tahun = 1945;
var hasil;

if (tanggal < 1 || tanggal > 31) {
	hasil = "Mohon masukan tanggal antara 1-31";
}else if (bulan < 1 || bulan > 12) {
	hasil = "Mohon masukan bulan antara 1-12";
}else if (tahun < 1900 || bulan > 2200) {
	hasil = "Mohon masukan Tahun antara 1900-2200";
}

switch (bulan) {
	case 1:
		bulan="Januari";
		break;
	case 2:
		bulan="Februari";
		break;
	case 3:
		bulan="Maret";
		break;
	case 4:
		bulan="April";
		break;
	case 5:
		bulan="Mei";
		break;
	case 6:
		bulan="Juni";
		break;
	case 7:
		bulan="Juli";
		break;
	case 8:
		bulan="Agustus";
		break;
	case 9:
		bulan="September";
		break;
	case 10:
		bulan="Oktober";
		break;
	case 11:
		bulan="November";
		break;
	case 12:
		bulan="Desember";
		break;
	default:
		console.log("");
		break;
}

if (hasil) {
	console.log(hasil);
}else{
	console.log(tanggal+' '+bulan+' '+ tahun);
}
